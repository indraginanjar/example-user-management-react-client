FROM node:14-alpine

RUN npm install -g tslint typescript
WORKDIR /app

COPY . /app/
# RUN yarn config set cache-folder .yarn
# RUN yarn install --pure-lockfile --cache-folder .yarn
RUN yarn install
# RUN yarn build

EXPOSE 3000

CMD ["yarn", "start"]

import React from 'react';
import UserModal from './UserModal';

class UserTable extends React.Component {
  constructor(props){
    super(props);

    this.getHeader = this.getHeader.bind(this);
    this.getRowsData = this.getRowsData.bind(this);
    this.getKeys = this.getKeys.bind(this);
  }
    
    getKeys = function(){
      return Object.keys(this.props.users);
    }
    
    getHeader = function() {

      var keys = this.getKeys();

      return keys.map(
        (key, index) => {
          return <th key={key}>{key.toUpperCase()}</th>
        }
      );
    }
    
    getRowsData = function() {
      var items = this.props.users;
      var keys = this.getKeys();
      return items.map((row, index) => {
        return <tr key={index}>
          <th scope="row">{index + 1}</th>
          <td>{row.username}</td>
          <td>{row.password}</td>
          <td>{row.name}</td>
          <td>
            <button className="btn btn-danger" onClick={() => {if(window.confirm("Delete user?")){this.removeToCollection(row.id);};}}>
              <i className="cil-delete"> Delete</i>
            </button>
          </td>
          </tr>
      })
    }
  
  render() {
    return (
     <div>
        <table className="table table-striped table-hover">
          <thead className="thead">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Username</th>
              <th scope="col">Password</th>
              <th scope="col">Name</th>
              <th scope="col">
              <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#userModal">
                <i className="cil-note-add" /> Add New User
              </button>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.getRowsData()}
          </tbody>
        </table>
        <div><UserModal /></div>
      </div>
    );
  }
}



export default UserTable;
